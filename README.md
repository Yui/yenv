# Yenv

[![nest badge](https://nest.land/badge.svg)](https://nest.land/package/Yenv)
[![Build Status](https://drone.external.sakura.yaikawa.com/api/badges/Yui/katsura/status.svg)](https://drone.external.sakura.yaikawa.com/Yui/yenv)
[![npm (scoped)](https://img.shields.io/npm/v/yuienv)](https://www.npmjs.com/package/yuienv)

- This module is aiming to be a more powerful version of standard dotenv

- This modules takes some things from [nodejs ts-dotenv module](https://github.com/LeoBakerHytch/ts-dotenv)

- You can see examples in `./examples` folder

## Node specific bug

- if you know why it happens tell me or make a pr
- values will be optional by default instead of required. To workaround this you need to set it explicitly like this:

```typescript
const env = await load({
  token: {
    type: String,
    optional: false, // <---
  },
});
```
