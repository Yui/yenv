import { build } from "https://deno.land/x/dnt@0.34.0/mod.ts";

await Deno.remove("npm", { recursive: true }).catch((_) => {});

await build({
  shims: {
    deno: true,
    timers: true,
    blob: true,
    undici: true,
  },
  entryPoints: ["./mod.ts"],
  outDir: "./npm",
  typeCheck: false,
  declaration: true,
  test: false,
  package: {
    name: "yuienv",
    version: Deno.args[0],
    description: "Helper for env vars and env files.",
    keywords: ["deno", "env"],
    author: "Yaikawa",
    license: "Apache License 2.0",
    bugs: {
      url: "https://codeberg.org/Yui/yenv/issues",
    },
    homepage: "https://codeberg.org/Yui/yenv",
    repository: {
      type: "git",
      url: "https://codeberg.org/Yui/yenv.git",
    },
    typesVersions: {
      "*": {
        "*": ["./types/mod.d.ts"],
      },
    },
  },
  compilerOptions: { target: "ES2020" },
});

// post build steps
Deno.copyFileSync("LICENSE", "npm/LICENSE");
Deno.copyFileSync("README.md", "npm/README.md");
