import { decode } from "https://deno.land/std@0.113.0/encoding/base64.ts";
import { Env } from "./parse.ts";
import { customEnv, envPlan, envPlanValue, finalEnv } from "./types/mod.ts";

export function verify<T extends envPlan>(parsed: Env, config: T): finalEnv {
  const vefiried: finalEnv = {};

  for (const [key, value] of Object.entries(config)) {
    const usable = makeUsable(value);
    const envVal = parsed[key];

    // Handle default checks for Boolean, Uint, Number, String
    // deno-lint-ignore no-explicit-any
    if ([Boolean, Uint8Array, Number, String].includes(usable.type as any)) {
      if (usable.default !== undefined && (envVal === "" || envVal == undefined)) {
        vefiried[key] = usable.default;
        continue;
      }

      if (usable.optional && envVal == undefined) {
        continue;
      }

      if (!envVal && !usable.default && !usable.optional) throw new Error(`Missing variable ${key}`);
    } else {
      // Handle default cheks for Enum, Regex, Bigint
      if (Array.isArray(usable.type) || "dotAll" in usable.type || usable.type == BigInt) {
        if (usable.default !== undefined && (envVal === "" || envVal == undefined)) {
          vefiried[key] = usable.default;
          continue;
        }

        if (usable.optional && envVal == undefined) {
          continue;
        }

        if (!envVal && !usable.default && !usable.optional) throw new Error(`Missing variable ${key}`);
      }
      // Function
    }

    switch (usable.type) {
      case Boolean:
        if (!["true", "false"].includes(envVal))
          throw new Error(`Invalid boolean option for env var '${key}'. Value '${envVal}' has to be 'true' or 'false'`);

        vefiried[key] = envVal === "true";
        break;
      case Uint8Array:
        vefiried[key] = decode(envVal);
        break;
      // deno-lint-ignore no-case-declarations
      case Number:
        const num = Number(envVal);
        if (isNaN(num)) throw new Error(`${key} is not a number.`);
        vefiried[key] = num;
        break;
      case String:
        vefiried[key] = envVal;
        break;
      default:
        // Handle enums
        if (Array.isArray(usable.type)) {
          if (!usable.type.includes(envVal))
            throw new Error(
              `Variable ${key} uses value ${envVal} which is not right. Supported values: ${usable.type
                .map((x) => x)
                .join(", ")}`
            );

          vefiried[key] = envVal;
          break;
        }
        // Handle Regex
        else if ("dotAll" in usable.type) {
          if (!usable.type.test(envVal))
            throw new Error(`Variable ${key} with value ${envVal} does not match regex ${usable.type}`);

          vefiried[key] = envVal;
          break;
          // Handle bigint
        } else if (usable.type == BigInt) {
          vefiried[key] = BigInt(envVal);
          break;
        }
        // Handle function
        else {
          vefiried[key] = (usable.type as customEnv)(envVal);
          break;
        }
    }
  }

  return vefiried;
}

export function makeUsable(val: envPlanValue) {
  return "type" in val
    ? {
        type: val.type,
        optional: "optional" in val ? val.optional : true,
        default: "default" in val ? val.default : undefined,
      }
    : {
        type: val,
        // Custom a.k.a function type are always optional. User is supposed to handle it in the fn.
        optional: typeof val === "function" ? true : false,
      };
}
